package pl.juido.dest_ster.dto

import java.math.RoundingMode
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class TemperaturesDataDto(
    val temperatures: List<TemperatureDto>,
    val alarms: List<AlarmSetupDto>,
    val runningAlarms: List<TemperatureDto>
) {


    fun runningAlarmsFormatted(): String {
        return (runningAlarms
            .map { t ->
                "czujnik: " + t.sensorName + " temperatura: " + t.value.setScale(
                    2
                )
            }
            .takeIf { it.isNotEmpty() }?.reduce { t1, t2 -> t1 + "\n" + t2 }
            ?: "Brak uruchomionych alarmów")
    }


    fun temperaturesFormatted(): String {
        return (temperatures
            .map { t ->
                "C: " + t.sensorName + " temperatura: " + t.value.setScale(
                    2,
                    RoundingMode.HALF_DOWN
                ) + " " +
                        ChronoUnit.SECONDS.between(
                            LocalDateTime.parse(t.timestamp),
                            LocalDateTime.now()
                        ) + " s"
            }
            .reduceOrNull { t1, t2 -> t1 + "\n" + t2 })
            .orEmpty()
    }
}