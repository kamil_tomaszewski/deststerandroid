package pl.juido.dest_ster.dto

import java.math.BigDecimal

class TemperatureDto(
    val id: Long,
    val sensorName: String,
    val value: BigDecimal,
    val timestamp: String
)