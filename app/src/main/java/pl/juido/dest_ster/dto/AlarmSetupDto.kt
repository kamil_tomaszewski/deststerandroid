package pl.juido.dest_ster.dto

import java.math.BigDecimal

class AlarmSetupDto(
    val id: Long,
    val active: Boolean,
    val temperatureDuringActivation: BigDecimal?,
    val sensorName: String,
    val minTemperature: BigDecimal?,
    val maxTemperature: BigDecimal?,
    val upTemperature: BigDecimal?,
    val downTemperature: BigDecimal?
) {

}