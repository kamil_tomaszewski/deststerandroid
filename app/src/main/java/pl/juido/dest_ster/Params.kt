package pl.juido.dest_ster

class Params(private val ips: List<String>) {

    private var errorId: Int = 0

    fun getBaseUrl(): String {
        return url() + "/api/"
    }

    fun url(): String {
        return "http://${this.ips[errorId % ips.size]}:8080"
    }

    fun error() {
        errorId++
    }
}