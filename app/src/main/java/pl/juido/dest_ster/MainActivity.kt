package pl.juido.dest_ster

import android.annotation.SuppressLint
import android.content.Context
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import org.json.JSONObject
import pl.juido.dest_ster.api_client.APIClient
import pl.juido.dest_ster.api_client.APIInterface
import pl.juido.dest_ster.command.CreateAlarmCommand
import pl.juido.dest_ster.dto.AlarmSetupDto
import pl.juido.dest_ster.dto.TemperaturesDataDto
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal


class MainActivity : AppCompatActivity() {

    private val interval: Long = 1000 * 5 //5 seconds
    private var context: Context? = null
    private var ringtoneSound: Ringtone? = null
    private var params: Params? = null
    private var lastTemperaturesData: TemperaturesDataDto? = null
    private var apiInterface: APIInterface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        updateParams()

        this.context = applicationContext

        val actualTemperatures = findViewById<TextView>(R.id.actualTemperatures)
        val runningAlarms = findViewById<TextView>(R.id.runningAlarms)

        val sensorNameEntry = findViewById<EditText>(R.id.sensorNameEntry)
        val minTemperatureEntry = findViewById<EditText>(R.id.minTemperatureEntry)
        val maxTemperatureEntry = findViewById<EditText>(R.id.maxTemperatureEntry)
        val upTemperatureEntry = findViewById<EditText>(R.id.upTemperatureEntry)
        val downTemperatureEntry = findViewById<EditText>(R.id.downTemperatureEntry)

        val tableLayout = findViewById<TableLayout>(R.id.table_layout_table)

        val addAlarmBtn = findViewById<Button>(R.id.addAlarm)

        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        ringtoneSound = RingtoneManager.getRingtone(context, ringtoneUri)

        addAlarmBtn.setOnClickListener {
            val sensorName = sensorNameEntry.text?.toString()
            if (sensorName.isNullOrBlank()) {
                return@setOnClickListener
            }
            val minTemperature =
                minTemperatureEntry.text?.toString()?.let { toBigDecimalOrNullIsBlank(it) }
            val maxTemperature =
                maxTemperatureEntry.text?.toString()?.let { toBigDecimalOrNullIsBlank(it) }
            val upTemperature =
                upTemperatureEntry.text?.toString()?.let { toBigDecimalOrNullIsBlank(it) }
            val downTemperature =
                downTemperatureEntry.text?.toString()?.let { toBigDecimalOrNullIsBlank(it) }

            Log.i(sensorName, minTemperature.toString())

            val command = CreateAlarmCommand(
                sensorName,
                minTemperature,
                maxTemperature,
                upTemperature,
                downTemperature
            )
            addAlarm(command)
        }

        val offAlarmSound = findViewById<Button>(R.id.offAlarmSound)
        offAlarmSound.setOnClickListener {
            ringtoneSound!!.stop()
        }

        initSetIpButton()

        updateTemperaturesJob(actualTemperatures, runningAlarms, tableLayout)
    }

    private fun initSetIpButton() {
        val setIpButton = findViewById<Button>(R.id.setIp)
        val ipsText = findViewById<EditText>(R.id.ips)
        ipsText.setText(getSavedIps())

        setIpButton.setOnClickListener {
            val ips = ipsText.text?.toString()
            if (ips != null && ips.isNotBlank()) {
                val sharedPref = getSharedPreferences("destSter", Context.MODE_PRIVATE)
                val editor = sharedPref.edit()
                editor.putString("ips", ips)
                editor.apply()
                updateParams()
            }
        }
    }

    private fun updateParams() {
        val ips = getSavedIps()
        val ipsList = ips.split(",").map { it.trim() }
        params = Params(ipsList)
        apiInterface = APIClient.create(params!!)!!.create(APIInterface::class.java)
    }

    private fun getSavedIps(): String {
        val sharedPref = getSharedPreferences("destSter", Context.MODE_PRIVATE)
        return sharedPref.getString("ips", "192.168.1.100")!!
    }

    private fun toBigDecimalOrNullIsBlank(it: String) = if (it.isBlank()) null else BigDecimal(it)

    @SuppressLint("SetTextI18n")
    private fun addAlarmToTable(alarm: AlarmSetupDto, tableLayout: TableLayout) {
        // Create a new table row.
        val tableRow = TableRow(context)

        // Set new table row layout parameters.
        val layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        tableRow.layoutParams = layoutParams

        val sensorName = TextView(context)
        sensorName.text = alarm.sensorName
        tableRow.addView(sensorName, 0)

        val min = TextView(context)
        min.text = alarm.minTemperature.toString()
        tableRow.addView(min, 1)

        val max = TextView(context)
        max.text = alarm.maxTemperature.toString()
        tableRow.addView(max, 2)

        val up = TextView(context)
        up.text = alarm.upTemperature.toString()
        tableRow.addView(up, 3)

        val down = TextView(context)
        down.text = alarm.downTemperature.toString()
        tableRow.addView(down, 4)

        val act = TextView(context)
        act.text = alarm.temperatureDuringActivation.toString()
        tableRow.addView(act, 5)

        val activate = CheckBox(context)
        activate.isChecked = alarm.active
        activate.setOnClickListener {
            changeAlarmActivate(alarm.id, activate.isChecked)
        }
        tableRow.addView(activate, 6)

        val delete = Button(context)
        delete.text = "usuń"
        delete.setOnClickListener {
            deleteAlarm(alarm.id)
        }
        tableRow.addView(delete, 7)

        tableLayout.addView(tableRow)
    }

    private fun addAlarm(command: CreateAlarmCommand) {

        val url = "alarm"
        val json = Gson().toJson(command)
        sendPostRequest(url, json)
    }

    private fun deleteAlarm(alarmId: Long) {
        val url = "alarm/$alarmId"
        sendDeleteRequest(url, "{}")
    }

    private fun changeAlarmActivate(alarmId: Long, activate: Boolean) {
        val postUrl = if (activate) "activate" else "deactivate"
        val url = "alarm/$alarmId/$postUrl"
        sendPutRequest(url, "{}")
    }

    private fun sendPostRequest(url: String, json: String) {
        sendRequest(Request.Method.POST, url, json)
    }

    private fun sendPutRequest(url: String, json: String) {
        sendRequest(Request.Method.PUT, url, json)
    }

    private fun sendDeleteRequest(url: String, json: String) {
        sendRequest(Request.Method.DELETE, url, json)
    }

    private fun sendRequest(requestMethod: Int, url: String, json: String) {
        val jsonObject = JSONObject(json)

        val request = JsonObjectRequest(
            requestMethod, params!!.getBaseUrl() + url, jsonObject,
            { response ->
                try {
                    Log.i("Response:", response.toString())
                } catch (e: Exception) {
                    Log.i("Exception:", e.message!!)
                }
            }, {
                Log.i("Volley error:", it.toString())
            })

        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 1f)

        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }

    private fun updateTemperaturesJob(
        actualTemperatures: TextView,
        runningAlarms: TextView,
        tableLayout: TableLayout
    ) {

        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed(object : Runnable {
            @SuppressLint("SetTextI18n")
            override fun run() {
                try {
                    val call: Call<TemperaturesDataDto> = apiInterface!!.doGetTemperatures()
                    call.enqueue(object : Callback<TemperaturesDataDto?> {
                        override fun onResponse(call: Call<TemperaturesDataDto?>, response: Response<TemperaturesDataDto?>) {
                            val temperaturesData: TemperaturesDataDto? = response.body()
                            if (temperaturesData != null) {
                                try {
                                    lastTemperaturesData = temperaturesData
                                    actualTemperatures.text =
                                        temperaturesData.temperaturesFormatted()

                                    if (temperaturesData.runningAlarms.isNotEmpty()) {
                                        ringtoneSound!!.play()
                                    }

                                    runningAlarms.text = temperaturesData.runningAlarmsFormatted()

                                    updateAlarms(temperaturesData, tableLayout)
                                } catch (e: RuntimeException) {
                                    Log.e("error on updateTemperaturesJob", e.message!!)
                                }
                            } else {
                                if (lastTemperaturesData != null) {
                                    actualTemperatures.text =
                                        lastTemperaturesData!!.temperaturesFormatted()
                                }

                            }
                        }

                        override fun onFailure(call: Call<TemperaturesDataDto?>, t: Throwable) {
                            call.cancel()
                        }
                    })

                } finally {
                    handler.postDelayed(this, interval)
                }
            }


            private fun updateAlarms(
                temperaturesData: TemperaturesDataDto,
                tableLayout: TableLayout
            ) {

                val count = tableLayout.childCount
                for (i in 2 until count) {
                    val child = tableLayout.getChildAt(i)
                    if (child is TableRow) (child as ViewGroup).removeAllViews()
                }

                temperaturesData.alarms.forEach { a -> addAlarmToTable(a, tableLayout) }
            }
        }, interval) //Every (2 seconds)
    }

}
