package pl.juido.dest_ster.api_client

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.juido.dest_ster.Params
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


private const val timeout = 400L

internal object APIClient {
    private var retrofit: Retrofit? = null
    fun create(params: Params): Retrofit? {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
//            .addInterceptor(interceptor)
            .connectTimeout(timeout, TimeUnit.MILLISECONDS)
            .readTimeout(timeout, TimeUnit.MILLISECONDS)
            .writeTimeout(timeout, TimeUnit.MILLISECONDS)
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(params.getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        return retrofit
    }
}
