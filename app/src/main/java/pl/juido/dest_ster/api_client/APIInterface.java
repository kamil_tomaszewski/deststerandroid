package pl.juido.dest_ster.api_client;

import pl.juido.dest_ster.dto.TemperaturesDataDto;
import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {

    @GET("/api/temperature")
    Call<TemperaturesDataDto> doGetTemperatures();

}
