package pl.juido.dest_ster.command

import java.math.BigDecimal

class CreateAlarmCommand(
    val sensorName: String,
    val minTemperature: BigDecimal?,
    val maxTemperature: BigDecimal?,
    val upTemperature: BigDecimal?,
    val downTemperature: BigDecimal?

) {
}
